/**
 * socket.io
 */

//Normal serial port
// var SerialPort = require('node-serialport').SerialPort;
// var sp = new SerialPort("/dev/ttyUSB0");

//var SerialPort = require("virtual-serialport");
// var SerialPort = require("serialport");

// var sp = new SerialPort("/dev/tty.usbserial", {
//   baudRate: 57600
// });

var status = "STATUS";

module.exports = function(io) {

const SerialPort = require('serialport');
const sp = new SerialPort('/dev/tty.usbserial', {
  baudRate:57600,
  databits:8,
  parity: 'none',
  stopbits:1,
  flowcontrol:'none'
}
);

const Readline = SerialPort.parsers.Readline;
const parser = sp.pipe(new Readline({ delimiter: '\r\n' }));
 

  io.on("connection", function(socket) {
    console.log(" Socket connected");

   
    parser.on('data', (data) => {
      io.emit("chat message",data);
    });

    socket.on("chat message", function(msg) {
      sp.write(msg+' \r');
      //sp.read();

      // emulate external device response
      //respond from raspebery pi or Arduino -- Equivalent to sp.emit("data", data)

      // setTimeout(function() {
      //   sp.writeToComputer("BLEEP! " + msg);
      // }, 1000);
    });
  });
};
