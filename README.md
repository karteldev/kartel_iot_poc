# Base poc project for serial port communication with express and websockets.
## features
- express.js
- socket.io
- sass/scss
- pug
- browserfy
- serialport
- virtualserialport


## set up
```bath
git clone https://ramuit44@bitbucket.org/karteldev/kartel_iot_poc.git kartel_iot_poc
cd kartel_iot_poc
yarn
yarn start
```

For now harcoded the serial port to /dev/ttyUSB0 -- since anyways it virtual . 

open some tab [http://localhost:4000](http://localhost:4000) and chat with serial port
